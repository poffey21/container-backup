#!/bin/bash

# Move a GitLab Container Registry
# Moves/rename a project while also migrating container images

# Copyright (c) 2021 Maxwell Power
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
# the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
# AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#### USAGE ####
# *** Ensure you have docker, curl, and jq installed and have enough space to store all your images before proceeding.***
# - If renaming a project in the same group: set `NEW_PROJECT`.
# - If renaming a project and moving it to an existing group: set `NEW_GROUP` and `NEW_PROJECT`.
# - If moving to an existing group: set `NEW_GROUP`.

# To restore images to the new location after repairing the projects manually.
# Rerun this script adding `restore` to the end of the command. eg. `moveProjectRegistry restore`.

# To remove local images after a successful project move.
# Rerun this script adding `remove` to the end of the command. eg. `moveProjectRegistry remove`.

###################################### WARNING #####################################################
#### This script is destructive as it will delete your existing container registry.             ####
#### To prevent complete loss of projects images, local images/tags are not removed by default. ####
####################################################################################################

##################################
######### User Settings ##########
##################################

## Old Project Values
EXISTING_GROUP=''   ## Required! If subgroup, enter the full path. Eg. `group/subgroup/subgroup...`
EXISTING_PROJECT='' ## Required!

## New Project Values
## Can set one or both
NEW_GROUP=''   ## Only set if moving groups
NEW_PROJECT='' ## Only set if renaming

## GitLab Values
GITLAB='gitlab.com'
REGISTRY='registry.gitlab.com'
API_TOKEN=''
USERNAME=''

## Danger
DELETE_IMAGES=0 ## Set to 1 to remove local images/tags
##################################

function downloadThenRemoveRegistry() {
  printf "${BOLD_GREEN_TEXT}""Downloading and Removing Registry""${RESET_TEXT}""\n"

  if [[ "$EXISTING_GROUP" == *\/* ]]; then
    EXISTING_GROUP_URLENCODED=$(sed 's#/#%2F#g' <<<$EXISTING_GROUP)
  else
    EXISTING_GROUP_URLENCODED=$EXISTING_GROUP
  fi
  if [[ "$NEW_GROUP" == *\/* ]]; then
    NEW_GROUP_URLENCODED=$(sed 's#/#%2F#g' <<<$NEW_GROUP)
  else
    NEW_GROUP_URLENCODED=$NEW_GROUP
  fi

  TOTALPAGES=$(curl --head -s --header "PRIVATE-TOKEN: $API_TOKEN" "https://$GITLAB/api/v4/projects/$EXISTING_GROUP_URLENCODED%2F$EXISTING_PROJECT/registry/repositories?per_page=100" | grep x-total-pages | head -n 1 | cut -d $' ' -f2)
  TOTALPAGES=$(echo "$TOTALPAGES" | tr -d '\r')

  for ((i = 0; i < TOTALPAGES; i++)); do

    for BRANCH in $(curl --header "PRIVATE-TOKEN: $API_TOKEN" "https://$GITLAB/api/v4/projects/$EXISTING_GROUP_URLENCODED%2F$EXISTING_PROJECT/registry/repositories??per_page=100&page=$i" | jq -r '.[].name'); do

      EXISTING_REGISTRYID=$(curl --header "PRIVATE-TOKEN: $API_TOKEN" "https://$GITLAB/api/v4/projects/$EXISTING_GROUP_URLENCODED%2F$EXISTING_PROJECT/registry/repositories" | jq ".[] | select (.name==\"$BRANCH\") | .id")
      EXISTING_TAGS=$(curl --header "PRIVATE-TOKEN: $API_TOKEN" "https://$GITLAB/api/v4/projects/$EXISTING_GROUP_URLENCODED%2F$EXISTING_PROJECT/registry/repositories/$EXISTING_REGISTRYID/tags" | jq -r '.[].name')
      for TAG in $EXISTING_TAGS; do
        printf "${BOLD_RED_TEXT}""Downloading Tag ""${TAG}"" from ""${EXISTING_PROJECT}""/""${BRANCH}"" to ""${NEW_PROJECT}""/""${BRANCH}""${RESET_TEXT}""\n"
        docker pull "$REGISTRY"/"$EXISTING_GROUP"/"$EXISTING_PROJECT"/"$BRANCH":"${TAG}" || exit 1
        if [ -z "$NEW_GROUP" ]; then
          docker tag "$REGISTRY"/"$EXISTING_GROUP"/"$EXISTING_PROJECT"/"$BRANCH":"${TAG}" $REGISTRY/$EXISTING_GROUP/$NEW_PROJECT/"$BRANCH":"${TAG}" || exit 1
        else
          docker tag "$REGISTRY"/"$EXISTING_GROUP"/"$EXISTING_PROJECT"/"$BRANCH":"${TAG}" $REGISTRY/$NEW_GROUP/$NEW_PROJECT/"$BRANCH":"${TAG}" || exit 1
        fi
        if [ $DELETE_IMAGES == 1 ]; then
          printf "${BOLD_RED_TEXT}""Deleting Local Tag ""${TAG}""${RESET_TEXT}""\n"
          docker image rm "$REGISTRY"/"$EXISTING_GROUP"/"$EXISTING_PROJECT"/"$BRANCH":"${TAG}"
        fi
        curl --header "PRIVATE-TOKEN: $API_TOKEN" --request "DELETE" "https://$GITLAB/api/v4/projects/$EXISTING_GROUP_URLENCODED%2F$EXISTING_PROJECT/registry/repositories/$EXISTING_REGISTRYID/tags/$TAG"
        printf "\n"
      done
      printf "${BOLD_RED_TEXT}""Removing Registry for Branch ""${BRANCH}""${RESET_TEXT}""\n"
      curl --header "PRIVATE-TOKEN: $API_TOKEN" --request "DELETE" "https://$GITLAB/api/v4/projects/$EXISTING_GROUP_URLENCODED%2F$EXISTING_PROJECT/registry/repositories/$EXISTING_REGISTRYID"
      printf "\n"
    done
  done
  printf "${BOLD_GREEN_TEXT}""DONE: Downloading and Removing Registry""${RESET_TEXT}""\n"

}

function moveProject() {
  printf "${BOLD_GREEN_TEXT}""Updating Project Path""${RESET_TEXT}""\n"
  if [ -z "$NEW_GROUP" ]; then
    printf "${BOLD_BLUE_TEXT}""Skipping Moving Group""${RESET_TEXT}""\n"
  else
    printf "${BOLD_RED_TEXT}""Moving Project ""${EXISTING_PROJECT}"" to Group ""${NEW_GROUP}""${RESET_TEXT}""\n"
    curl --header "PRIVATE-TOKEN: $API_TOKEN" --request "PUT" "https://$GITLAB/api/v4/projects/$EXISTING_GROUP_URLENCODED%2F$EXISTING_PROJECT/transfer?namespace=$NEW_GROUP"
    printf "\n"
  fi
  if [ -z "$NEW_PROJECT" ]; then
    printf "${BOLD_BLUE_TEXT}""Skipping Renaming Project""${RESET_TEXT}""\n"
  else
    if [ -z "$NEW_GROUP" ]; then
      printf "${BOLD_RED_TEXT}""Renaming Project ""${EXISTING_PROJECT}"" to ""${NEW_PROJECT}""${RESET_TEXT}""\n"
      curl --header "PRIVATE-TOKEN: $API_TOKEN" --request PUT "https://gitlab.com/api/v4/projects/$EXISTING_GROUP_URLENCODED%2F$EXISTING_PROJECT?path=$NEW_PROJECT"
      curl --header "PRIVATE-TOKEN: $API_TOKEN" --request PUT "https://gitlab.com/api/v4/projects/$EXISTING_GROUP_URLENCODED%2F$NEW_PROJECT?name=$NEW_PROJECT"
      printf "\n"
    else
      printf "${BOLD_RED_TEXT}""Renaming Project ""${EXISTING_PROJECT}"" to ""${NEW_PROJECT}""${RESET_TEXT}""\n"
      curl --header "PRIVATE-TOKEN: $API_TOKEN" --request PUT "https://gitlab.com/api/v4/projects/$NEW_GROUP_URLENCODED%2F$EXISTING_PROJECT?path=$NEW_PROJECT"
      curl --header "PRIVATE-TOKEN: $API_TOKEN" --request PUT "https://gitlab.com/api/v4/projects/$NEW_GROUP_URLENCODED%2F$NEW_PROJECT?name=$NEW_PROJECT"
      printf "\n"
    fi
  fi
  printf "${BOLD_GREEN_TEXT}""DONE: Updating Project Path""${RESET_TEXT}""\n"

}

function restoreRegistry() {
  printf "${BOLD_GREEN_TEXT}""Restoring Registry""${RESET_TEXT}""\n"
  if [ -z "$NEW_GROUP" ]; then
    TAGS=$(docker images "$REGISTRY"/"$EXISTING_GROUP"/"$NEW_PROJECT"/* --format "{{.Repository}}:{{.Tag}}")
  else
    TAGS=$(docker images "$REGISTRY"/"$NEW_GROUP"/"$NEW_PROJECT"/* --format "{{.Repository}}:{{.Tag}}")
  fi
  for TAG in $TAGS; do
    printf "${BOLD_RED_TEXT}""Restoring Tag ""${TAG}""${RESET_TEXT}""\n"
    docker push "$TAG"
    if [ $DELETE_IMAGES == 1 ]; then
      printf "${BOLD_RED_TEXT}""Deleting Tag ""${TAG}""${RESET_TEXT}""\n"
      docker image rm "$TAG"
    fi
  done
  printf "${BOLD_GREEN_TEXT}""DONE: Restoring Registry""${RESET_TEXT}""\n"

}

function deleteLocalImages() {
  printf "${BOLD_GREEN_TEXT}""Removing Images""${RESET_TEXT}""\n"

  TAGS=$(docker images "$REGISTRY"/"$EXISTING_GROUP"/"$NEW_PROJECT"/* --format "{{.Repository}}:{{.Tag}}")
  TAGS+=' '
  TAGS+=$(docker images "$REGISTRY"/"$NEW_GROUP"/"$NEW_PROJECT"/* --format "{{.Repository}}:{{.Tag}}")
  TAGS+=' '
  TAGS+=$(docker images "$REGISTRY"/"$EXISTING_GROUP"/"$EXISTING_PROJECT"/* --format "{{.Repository}}:{{.Tag}}")

  for TAG in $TAGS; do
    printf "${BOLD_RED_TEXT}""Deleting Tag ""${TAG}""${RESET_TEXT}""\n"
    docker image rm "$TAG"
  done

  printf "${BOLD_GREEN_TEXT}""DONE: Removing Images""${RESET_TEXT}""\n"
}

# Set Text Colours
BOLD_RED_TEXT='\033[1;31m' BOLD_BLUE_TEXT='\033[1;34m' BOLD_GREEN_TEXT='\033[1;32m' RESET_TEXT='\033[0m'

if [ "$1" == 'restore' ]; then
  docker login "$REGISTRY" -u "$USERNAME" -p "$API_TOKEN" >/dev/null 2>&1
  restoreRegistry
elif [ "$1" == 'remove' ]; then
  deleteLocalImages
else
  printf "\n""${BOLD_GREEN_TEXT}""##########\n# Ready! #\n##########""${RESET_TEXT}""\n\n"
  printf "This process will download and destroy your existing registry, move your project, then restore.\nThis is your last chance to exit.\n\n"
  printf "${BOLD_RED_TEXT}""When ready, Press [ENTER] to continue""${RESET_TEXT}""\n"
  read -r READY

  docker login "$REGISTRY" -u "$USERNAME" -p "$API_TOKEN" >/dev/null 2>&1
  downloadThenRemoveRegistry
  moveProject
  restoreRegistry

  printf "${BOLD_GREEN_TEXT}""DONE: Process Complete""${RESET_TEXT}""\n"
fi
